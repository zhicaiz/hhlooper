# HHLooper
HHLooper

# Setup

first time setup (on cori):

```
module load root python
conda create --name venv_root python=3.9
source activate venv_root
conda install numpy
git clone ssh://git@gitlab.cern.ch:7999/zhicaiz/hhlooper.git HHLooper
cd HHLooper
make
```

every time login:

```
module load root python
source activate venv_root
```

# About  the looper:

All the selections are defined  in app/HHLooper.cc in the define cuts block

All histograms are defined in app/HHLooper.cc in the histograms block

Each output histogram will have the name of `CutName__HistogramName`, i.e. by default the program saves all histograms for all cuts


# Run looper

```
./process.sh 20220622v1 

```

The argument to this script is a (random) label you set for the directory to store the output histograms

# Make plots

```
cd python
./plot.sh ../hists/20220622/all/
```

The argument to this script is the directory where the histograms are stored
