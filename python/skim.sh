
inputDir=$1
outputDir=${inputDir/samples/samples_skim}

for idir in `ls ${inputDir}/`
do
    echo $idir
    mkdir -p ${outputDir}
    mkdir -p ${outputDir}/${idir}
    for ifile in `ls ${inputDir}/${idir}/`
    do
        echo python skim_append_weight.py ${inputDir}/${idir}/$ifile ${outputDir}/${idir}/$ifile 
        python skim_append_weight.py ${inputDir}/${idir}/$ifile ${outputDir}/${idir}/$ifile 
    done
done
