from ROOT import *

import sys
import os


def GetKeyNames( self, dir = "" ):
        self.cd(dir)
        return [key.GetName() for key in gDirectory.GetListOfKeys()]
def GetClassNames( self, dir = "" ):
        self.cd(dir)
        return [key.GetClassName() for key in gDirectory.GetListOfKeys()]

TFile.GetKeyNames = GetKeyNames
TFile.GetClassNames = GetClassNames

if __name__ == '__main__':
    inputFile = "/global/projecta/projectdirs/atlas/cecily/Ntuple_h026_0530/nti_all.root" 
    if len(sys.argv) > 1:
        inputFile = sys.argv[1]
    outFile = "/global/projecta/projectdirs/atlas/zhicaiz/HH/samples/Ntuple_h026_0630/nti_all_skim.root"
    if len(sys.argv) > 2:
        outFile = sys.argv[2]

    os.system("rm "+outFile)

    cut = "nboostedJets > 1 && boostedJets_Xbb2020v3_Higgs[0]>0.2 && boostedJets_Xbb2020v3_Higgs[1]>0.2"

    print("input: "+inputFile)
    print("output: "+outFile)
    print("cut: "+cut)

    inFile = TFile(inputFile, "READ")

    keyList = inFile.GetKeyNames()
    classList = inFile.GetClassNames()
    
    outFile = TFile(outFile, "RECREATE")
    outFile.cd()

    for j in range(0, len(keyList)):
        print(classList[j] + "   ===   " + keyList[j])
        if classList[j] == "TTree":
            inFile.cd()
            inputTree = inFile.Get(keyList[j])
            print("events before cut: "+str(inputTree.GetEntries()))
            outFile.cd()
            outputTree = inputTree.CopyTree(cut)
            outputTree.Write()
            print("events after cut: "+str(outputTree.GetEntries()))
        if classList[j] == "TH1F" or classList[j] == "TH1D":
            inFile.cd()
            histThis = inFile.Get(keyList[j])
            outFile.cd()
            histThis_out = histThis.Clone()
            histThis_out.Write()
    outFile.Close()


    
