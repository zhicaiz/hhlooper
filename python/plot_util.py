from __future__ import print_function
import numpy as np
import math
import ROOT as r
import shlex
import sys
import os

leftMargin   = 0.13
rightMargin  = 0.01
topMargin    = 0.07
bottomMargin = 0.12
r.gStyle.SetOptStat(0)
r.gStyle.SetOptFit(111)
r.gStyle.SetStatX(0.99)
r.gStyle.SetStatY(0.9)
r.gStyle.SetStatW(0.2)
r.gStyle.SetStatH(0.15)
r.gROOT.SetBatch(1)

r.gROOT.SetStyle("ATLAS")

def ATLASLabel(x, y, lumi=139, color=1):
    l = r.TLatex(x, y, "ATLAS")
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextColor(color)
    l.Draw()

    p = r.TLatex(x+0.1, y, "Internal #sqrt{s}= 13 TeV #int L dt = %d"%lumi+" fb^{-1}")
    p.SetNDC()
    p.SetTextFont(42)
    p.SetTextColor(color)
    p.Draw()

def significance(s, b):
    if s <= 1e-8 or b <= 1e-8:
        return 0.0
    if (s+b)*math.log(1.0+s/b) - s < 0.0:
        return 0.0
    return math.sqrt(2.0*((s+b)*math.log(1.0+s/b) - s))

def upperLimit(s, b):
    if s <= 1e-8 or b <= 1e-8:
        return 99999.9
    #Neyman procedure
    #https://pdg.lbl.gov/2021/reviews/rpp2020-rev-statistics.pdf, page 27, eq. 40.76b
    return (r.TMath.ChisquareQuantile(0.95, 2.0*(b+1.0))*0.5-b)/s

def removeLowHigh(hist, xlow=None, xhigh=None):
    binlow = 0
    if xlow != None:
        binlow = hist.FindBin(xlow)
        for ibin in range(binlow):
            hist.SetBinContent(ibin, 0.0)
    nbins = hist.GetNbinsX()
    binhigh = nbins
    if xhigh != None:
        binhigh = hist.FindBin(xhigh)
        for ibin in range(binhigh, nbins+1):
            hist.SetBinContent(ibin, 0.0)
    return hist

def rebin(hists, nbin):
    for hist in hists:
        if not hist: continue
        currnbin = hist.GetNbinsX()
        fac = currnbin / nbin
        if float(fac).is_integer() and fac > 0:
            hist.Rebin(int(fac))
def remove_underflow(hists):
    def func(hist):
        hist.SetBinContent(0, 0)
        hist.SetBinError(0, 0)
    if isinstance(hists, list):
        for hist in hists:
            func(hist)
    else:
        func(hists)
    return hists
def remove_overflow(hists):
    def func(hist):
        hist.SetBinContent(hist.GetNbinsX()+1, 0)
        hist.SetBinError(hist.GetNbinsX()+1, 0)
    if isinstance(hists, list):
        for hist in hists:
            func(hist)
    else:
        func(hists)
    return hists

def add_underflow(hists):
    def func(hist):
        hist.SetBinContent(1, hist.GetBinContent(0)+hist.GetBinContent(1))
        hist.SetBinError(1, math.sqrt(hist.GetBinError(0)*hist.GetBinError(0)+hist.GetBinError(1)*hist.GetBinError(1)))
    if isinstance(hists, list):
        for hist in hists:
            func(hist)
    else:
        func(hists)
    return hists
def add_overflow(hists):
    def func(hist):
        nx=hist.GetNbinsX()
        hist.SetBinContent(nx, hist.GetBinContent(nx)+hist.GetBinContent(nx+1))
        hist.SetBinError(nx, math.sqrt(hist.GetBinError(nx)*hist.GetBinError(nx)+hist.GetBinError(nx+1)*hist.GetBinError(nx+1)))
    if isinstance(hists, list):
        for hist in hists:
            func(hist)
    else:
        func(hists)
    return hists

def blind_data(hists):
    def func(hist):
        nx=hist.FindBin(125.0)
        hist.SetBinContent(nx, 0.0)
        hist.SetBinError(nx, 0.0)
        hist.SetBinContent(nx-1, 0.0)
        hist.SetBinError(nx-1, 0.0)
        hist.SetBinContent(nx+1, 0.0)
        hist.SetBinError(nx+1, 0.0)
    if isinstance(hists, list):
        for hist in hists:
            func(hist)
    else:
        func(hists)
    return hists

def add_underflow(hists):
    def func(hist):
        hist.SetBinContent(1, hist.GetBinContent(1)+hist.GetBinContent(0))
        hist.SetBinError(1, math.sqrt(hist.GetBinError(1)*hist.GetBinError(1)+hist.GetBinError(0)*hist.GetBinError(0)))
    if isinstance(hists, list):
        for hist in hists:
            func(hist)
    else:
        func(hists)
    return hists



def makeplot_single_2d(
    sig_fnames_=None,
    bkg_fnames_=None,
    data_fname_=None,
    sig_legends_=None,
    bkg_legends_=None,
    sig_colors_=None,
    bkg_colors_=None,
    hist_name_=None,
    dir_name_="plots",
    output_name_=None,
    extraoptions=None
    ):

    if sig_fnames_ == None or bkg_fnames_ == None or hist_name_ == None or sig_legends_ == None or bkg_legends_ == None:
        print("nothing to plot....")
        return
    print("making plot for "+hist_name_)
    for idx in range(len(sig_legends_)):
        sig_legends_[idx] = sig_legends_[idx].split(" x ")[0]
    for idx in range(len(bkg_legends_)):
        bkg_legends_[idx] = bkg_legends_[idx].split(" x ")[0]
   
    s_color = [632, 617, 839, 800, 1]
    b_color = [920, 2007, 2005, 2003, 2001, 2011]
    if sig_colors_:
        s_color = sig_colors_
    if bkg_colors_:
        b_color = bkg_colors_
    
    tfs_sig = {}
    h2_sig = []
    maxY = 0.0
    for idx in range(len(sig_fnames_)): 
        fn = sig_fnames_[idx]
        n = os.path.basename(fn.replace(".root", ""))
        tfs_sig[n] = r.TFile(fn)
        h2 = tfs_sig[n].Get(hist_name_)
        h2.SetName(hist_name_+"_sig_"+str(idx))
        h2.SetMarkerColor(s_color[idx])
        h2.SetMarkerStyle(7)
        #h2.SetMarkerSize(0.1)
        #print(h2.Integral())
        h2_sig.append(h2)
     
    tfs_bkg = {}
    h2_bkg = []
    for idx in range(len(bkg_fnames_)): 
        fn = bkg_fnames_[idx]
        n = os.path.basename(fn.replace(".root", ""))
        tfs_bkg[n] = r.TFile(fn)
        h2 = tfs_bkg[n].Get(hist_name_)
        h2.SetName(hist_name_+"_bkg_"+str(idx))
        h2.SetMarkerColor(b_color[idx])
        h2.SetMarkerStyle(7)
        #h2.SetMarkerSize(0.1)
        #print(h2.Integral())
        h2_bkg.append(h2)
    h2_data = None 

    tfs_data = {}
    if data_fname_:
        tfs_data["fdata"] = r.TFile(data_fname_)
        h2_data = tfs_data["fdata"].Get(hist_name_)
        h2_data.SetName(hist_name_+"_data")
        h2_data.SetMarkerColor(1)
        h2_data.SetMarkerStyle(7)
        #print(h2_data.Integral())
    myC = r.TCanvas("myC","myC", 600, 600)
    myC.SetTicky(1)
    myC.SetRightMargin( 0.05 )
    myC.SetLeftMargin( leftMargin ) 

    
    h2_sig[0].Draw("scat=10.0")
    for idx in range(1, len(h2_sig)):
        h2_sig[idx].Draw("same scat=10.0")
    for idx in range(len(h2_bkg)):
        h2_bkg[idx].Draw("same scat=10.0")

    h2_sig[0].SetTitle("")
   
    if h2_data:
        h2_data.Draw("same scat=10.0")
    #h2_sig[0].GetZaxis().SetTitle("Events")
    h2_sig[0].GetYaxis().SetTitleOffset(1.1)
    h2_sig[0].GetYaxis().SetTitleSize(0.055)
    h2_sig[0].GetYaxis().SetLabelSize(0.045)
    h2_sig[0].GetYaxis().CenterTitle()

    leg = r.TLegend(leftMargin, 0.65, leftMargin+0.20, 0.85)
    #leg.SetNColumns(2)
    #leg.SetFillStyle(1)
    leg.SetFillColor(0)
    leg.SetBorderSize(1)
    leg.SetTextFont(42)
    leg.SetTextSize(0.07)
    for idx in range(len(h2_sig)):
        leg.AddEntry(h2_sig[idx], sig_legends_[idx], "p")
    for idx in range(len(h2_bkg)):
        leg.AddEntry(h2_bkg[idx], bkg_legends_[idx], "p")
    if h2_data:
        leg.AddEntry(h2_data, "Data", "p")
    leg.Draw()
    h2_sig[0].GetXaxis().SetTitleOffset(0.8)
    h2_sig[0].GetXaxis().SetTitleSize(0.055)
    h2_sig[0].GetXaxis().SetLabelSize(0.045)
    h2_sig[0].GetXaxis().CenterTitle()

    if "xaxis_label" in extraoptions and extraoptions["xaxis_label"] != None:
        x_title = extraoptions["xaxis_label"]
        h2_sig[0].GetXaxis().SetTitle(x_title)
    if "yaxis_label" in extraoptions and extraoptions["yaxis_label"] != None:
        y_title = extraoptions["yaxis_label"]
        h2_sig[0].GetYaxis().SetTitle(y_title)

    ##########draw ATLAS 
     
    lumi_value = 139
    if "lumi_value" in extraoptions:
        lumi_value = extraoptions["lumi_value"]
 
    text1 = r.TLatex(leftMargin+0.03, 0.85, "ATLAS")
    text1.SetNDC()
    text1.SetTextFont(72)
    text1.SetTextSize(0.070)
    text1.Draw()

    text2 = r.TLatex(leftMargin+0.2, 0.85, "Internal   #sqrt{s}= 13 TeV   #int L dt = %d"%lumi_value+" fb^{-1}")
    text2.SetNDC()
    text2.SetTextFont(42)
    text2.SetTextSize(0.055)
    text2.Draw()

    #ATLASLabel(leftMargin+0.1, 0.85, lumi_value)

    outFile = dir_name_
    if output_name_:
        outFile = outFile + "/" +output_name_
    else:
        outFile = outFile + "/" + hist_name_

    
    myC.SaveAs(outFile+".png")
    myC.SaveAs(outFile+".pdf")
    myC.SaveAs(outFile+".C")
 
def makeplot_fitmass(
    data_fname_=None,
    hist_name_=None,
    dir_name_="plots",
    output_name_=None,
    extraoptions=None
    ):

    if data_fname_ == None or hist_name_ == None: 
        print("nothing to plot....")
        return
    if "msoft" not in hist_name_:
        return
    print("making plot for "+hist_name_)
    print(data_fname_)
   
    tfs_data = r.TFile(data_fname_)
    h1_data = tfs_data.Get(hist_name_)
    h1_data.SetName(hist_name_+"_data")
    h1_data.SetBinErrorOption(1)
    h1_data.SetLineColor(1)
    h1_data.SetLineWidth(2)
    h1_data.SetMarkerColor(1)
    h1_data.SetMarkerStyle(20)
    #print(h1_data.Integral())
    if "nbins" in extraoptions:
        rebin([h1_data], extraoptions["nbins"])   
    if "remove_underflow" in extraoptions:
        if extraoptions["remove_underflow"]:
            remove_underflow([h1_data])
    if "remove_overflow" in extraoptions:
        if extraoptions["remove_overflow"]:
            remove_overflow([h1_data])
    myC = r.TCanvas("myC","myC", 600, 600)
    myC.SetTicky(1)
    myC.SetRightMargin( rightMargin )
    myC.SetLeftMargin( leftMargin ) 
    myC.SetBottomMargin(0.14)

   
    h1_data.SetTitle("")
    maxY = h1_data.GetMaximum()
    h1_data.Draw("PEX0")
    h1_data.GetYaxis().SetTitle("Events")
    h1_data.GetYaxis().SetTitleOffset(0.85)
    h1_data.GetYaxis().SetTitleSize(0.07)
    h1_data.GetYaxis().SetLabelSize(0.045)
    h1_data.GetYaxis().CenterTitle()

    h1_data.GetXaxis().SetTitleOffset(0.94)
    h1_data.GetXaxis().SetTitleSize(0.06)
    h1_data.GetXaxis().SetLabelSize(0.045)
    h1_data.GetXaxis().SetLabelOffset(0.013)

    if "xaxis_label" in extraoptions and extraoptions["xaxis_label"] != None:
        x_title = extraoptions["xaxis_label"]
        h1_data.GetXaxis().SetTitle(x_title)

    f1 = r.TF1("gaus", "gaus(0)", 50, 170)
    f1.SetLineColor(r.kRed)
    h1_data.Fit(f1, "", "", 50, 170)
   
    N_bkg = f1.Integral(95.0, 135.0)/h1_data.GetBinWidth(2)
    tex_nb = r.TLatex(0.20,0.80,"N_{b} = #int_{95}^{135} = %6.2f"%N_bkg)
    tex_nb.SetNDC()
    tex_nb.SetTextFont(42)
    tex_nb.SetTextSize(0.035)
    tex_nb.SetLineWidth(2)
    tex_nb.Draw()

    ##########draw ATLAS
   
    lumi_value = 137
    if "lumi_value" in extraoptions:
        lumi_value = extraoptions["lumi_value"]
    #ATLASLabel(leftMargin+0.1, 0.85, lumi_value)
 
    text1 = r.TLatex(leftMargin+0.03, 0.85, "ATLAS")
    text1.SetNDC()
    text1.SetTextFont(72)
    text1.SetTextSize(0.070)
    text1.Draw()

    text2 = r.TLatex(leftMargin+0.2, 0.85, "Internal   #sqrt{s}= 13 TeV   #int L dt = %d"%lumi_value+" fb^{-1}")
    text2.SetNDC()
    text2.SetTextFont(42)
    text2.SetTextSize(0.055)
    text2.Draw()


    outFile = dir_name_
    if output_name_:
        outFile = outFile + "/" +output_name_
    else:
        outFile = outFile + "/" + hist_name_
    #print("maxY = "+str(maxY))
    h1_data.SetMaximum(maxY*1.5)
    
    myC.SaveAs(outFile+"_fit.png")
    myC.SaveAs(outFile+"_fit.pdf")
    myC.SaveAs(outFile+"_fit.C")
  
def makeplot_single(
    sig_fnames_=None,
    bkg_fnames_=None,
    data_fname_=None,
    sig_legends_=None,
    bkg_legends_=None,
    sig_colors_=None,
    bkg_colors_=None,
    hist_name_=None,
    sig_scale_=1.0,
    dir_name_="plots",
    output_name_=None,
    extraoptions=None
    ):

    if sig_fnames_ == None or bkg_fnames_ == None or hist_name_ == None or sig_legends_ == None or bkg_legends_ == None:
        print("nothing to plot....")
        return

    if "_v_" in hist_name_:
        makeplot_single_2d(sig_fnames_=sig_fnames_, bkg_fnames_ = bkg_fnames_, data_fname_=data_fname_, sig_legends_=sig_legends_, bkg_legends_=bkg_legends_, sig_colors_=sig_colors_, bkg_colors_=bkg_colors_, hist_name_=hist_name_, dir_name_=dir_name_, extraoptions=extraoptions)
        return

    print("making plot for "+hist_name_)
    print(sig_fnames_)
    print(bkg_fnames_)
   
    s_color = [632, 617, 839, 800, 1]
    b_color = [920, 2007, 2005, 2003, 2001, 2011]
    if sig_colors_:
        s_color = sig_colors_
    if bkg_colors_:
        b_color = bkg_colors_
    
    tfs_sig = {}
    h1_sig = []
    maxY = 0.0
    if data_fname_ != None:
        extraoptions["stack_signal"]=True
        sig_scale_ = 1.0
    if "stack_signal" in extraoptions and extraoptions["stack_signal"]:
        sig_scale_ = 1.0
        for idx in range(len(sig_legends_)):
            sig_legends_[idx] = sig_legends_[idx].split(" x ")[0]
    for idx in range(len(sig_fnames_)): 
        fn = sig_fnames_[idx]
        n = os.path.basename(fn.replace(".root", ""))
        tfs_sig[n] = r.TFile(fn)
        h1 = tfs_sig[n].Get(hist_name_)
        h1.SetName(hist_name_+"_sig_"+str(idx))
        h1.Scale(sig_scale_)
        h1.SetLineWidth(2)
        h1.SetLineColor(s_color[idx])
        print("signal index and sum:",idx, h1.Integral())
        h1_sig.append(h1)
     
    tfs_bkg = {}
    h1_bkg = []
    for idx in range(len(bkg_fnames_)): 
        fn = bkg_fnames_[idx]
        n = os.path.basename(fn.replace(".root", ""))
        tfs_bkg[n] = r.TFile(fn)
        print("fn hist_name_",fn,hist_name_)
        h1 = tfs_bkg[n].Get(hist_name_)
        print("sum 1:",hist_name_,h1.Integral())
        h1.SetName(hist_name_+"_bkg_"+str(idx))
        h1.SetLineWidth(2)
        h1.SetLineColor(b_color[idx])
        h1.SetFillColorAlpha(b_color[idx], 1)
        print("sum 2:",hist_name_,h1.Integral())
        h1_bkg.append(h1)
    h1_data = None 

    tfs_data = {}
    if data_fname_:
        tfs_data["fdata"] = r.TFile(data_fname_)
        h1_data = tfs_data["fdata"].Get(hist_name_)
        h1_data.SetName(hist_name_+"_data")
        h1_data.SetBinErrorOption(1)
        h1_data.SetLineColor(1)
        h1_data.SetLineWidth(2)
        h1_data.SetMarkerColor(1)
        h1_data.SetMarkerStyle(20)
        print("sum data: ",h1_data.Integral())
    if "nbins" in extraoptions:
        nbins_rebin = extraoptions["nbins"]
        if "_m_mgg" in hist_name_:
            nbins_rebin = 55
        rebin(h1_bkg, nbins_rebin)   
        rebin(h1_sig, nbins_rebin)
        if h1_data:
            rebin([h1_data], nbins_rebin)
    if "remove_underflow" in extraoptions:
        if extraoptions["remove_underflow"]:
            remove_underflow(h1_sig)
            remove_underflow(h1_bkg)
            if h1_data:
                remove_underflow([h1_data])
    if "remove_overflow" in extraoptions:
        if extraoptions["remove_overflow"]:
            remove_overflow(h1_sig)
            remove_overflow(h1_bkg)
            if h1_data:
                remove_overflow([h1_data])
    if "add_overflow" in extraoptions:
        if extraoptions["add_overflow"]:
            add_overflow(h1_sig)
            add_overflow(h1_bkg)
            if h1_data:
                add_overflow([h1_data])
    if "add_underflow" in extraoptions:
        if extraoptions["add_underflow"]:
            add_underflow(h1_sig)
            add_underflow(h1_bkg)
            if h1_data:
                add_underflow([h1_data])
    if "blind_data" in extraoptions:
        if extraoptions["blind_data"]:
            #blind_data(h1_sig)
            #blind_data(h1_bkg)
            if h1_data:
                blind_data([h1_data])

    myC = r.TCanvas("myC","myC", 600, 600)
    myC.SetTicky(1)
    pad1 = r.TPad("pad1","pad1", 0.05, 0.33,0.95, 0.97) 
    pad1.SetBottomMargin(0)
    pad1.SetRightMargin( rightMargin )
    pad1.SetLeftMargin( leftMargin ) 
    pad2 = r.TPad("pad2","pad2", 0.05, 0.045, 0.95, 0.33)
    pad2.SetBottomMargin(0.4)
    pad2.SetTopMargin(0.008)
    pad2.SetRightMargin( rightMargin )
    pad2.SetLeftMargin( leftMargin )

    pad2.Draw()
    pad1.Draw()

    pad1.cd()

    stack = r.THStack("stack", "stack")
    #signal process exist
    #hist_all = h1_sig[0].Clone("hist_all")
    #hist_all.Scale(1.0/sig_scale_)
    hist_all = h1_bkg[0].Clone("hist_all")
    hist_all.Scale(0.0)

    #no signal process
    hist_s = hist_all.Clone("hist_s")
    #signal process exist
    #hist_s = h1_sig[0].Clone("hist_s")

    hist_b = h1_bkg[0].Clone("hist_b")
    for idx in range(len(h1_bkg)):
        print("stack add:", h1_bkg[idx].Integral())
        stack.Add(h1_bkg[idx])
        hist_all.Add(h1_bkg[idx])
        if idx > 0:
            hist_b.Add(h1_bkg[idx])
    for idx in range(len(h1_sig)):
        if idx > -1:
            hist_temp = h1_sig[idx].Clone(h1_sig[idx].GetName()+"_temp")
            hist_temp.Scale(1.0/sig_scale_)
            hist_all.Add(hist_temp)
            hist_s.Add(h1_sig[idx])

    stack.SetTitle("")
    if "stack_signal" in extraoptions and extraoptions["stack_signal"]:
        for idx in range(len(h1_sig)):
            h1_sig[idx].SetFillColorAlpha(s_color[idx], 1)
            stack.Add(h1_sig[idx])
        if stack.GetMaximum() > maxY:
            maxY = stack.GetMaximum()
        stack.Draw("hist")
    else:
        stack.Draw("hist")
        if stack.GetMaximum() > maxY:
            maxY = stack.GetMaximum()
        for idx in range(len(h1_sig)):
            if h1_sig[idx].GetMaximum() > maxY:
                maxY = h1_sig[idx].GetMaximum()
            h1_sig[idx].Draw("samehist")
    
    if h1_data:
        if h1_data.GetMaximum() > maxY:
            maxY = h1_data.GetMaximum()
        h1_data.Draw("samePEX0")
    stack.GetYaxis().SetTitle("Events")
    stack.GetYaxis().SetTitleOffset(0.88)
    stack.GetYaxis().SetTitleSize(0.08)
    stack.GetYaxis().SetLabelSize(0.045)
    stack.GetYaxis().CenterTitle()
    #if "xaxis_range" in extraoptions:
    #    stack.GetXaxis().SetRangeUser(float(extraoptions["xaxis_range"][0]),float(extraoptions["xaxis_range"][1]))

    leg = r.TLegend(0.16, 0.60, 0.97, 0.82)
    leg.SetNColumns(3)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.07)
    
    for idx in range(len(h1_bkg)):
        leg.AddEntry(h1_bkg[idx], bkg_legends_[idx], "F")
    for idx in range(len(h1_sig)):
        if "stack_signal" in extraoptions and extraoptions["stack_signal"]:
            leg.AddEntry(h1_sig[idx], sig_legends_[idx], "F")
        else:
            leg.AddEntry(h1_sig[idx], sig_legends_[idx], "L")
    if h1_data:
        leg.AddEntry(h1_data, "Data", "ep")

    leg.Draw()
    pad2.cd()
    pad2.SetGridy(1)
    ratio = None
    ratio_Low  = 0.0
    ratio_High  = 2.0
    if h1_data:
        ratio = h1_data.Clone("ratio_data_over_mc")
        ratio.Divide(hist_all)
        if "ratio_range" in extraoptions:
            ratio_Low = extraoptions["ratio_range"][0]
            ratio_High = extraoptions["ratio_range"][1]
        ratio.GetYaxis().SetTitle("data/mc")
        ratio.GetYaxis().SetRangeUser(ratio_Low, ratio_High)
        ratio.Draw("same PE")
    else:
        ratio = h1_sig[0].Clone("ratio")
        ratio_High = 0.0
        for ibin in range(1,ratio.GetNbinsX()+1):
            s = hist_s.GetBinContent(ibin) / sig_scale_
            b = hist_b.GetBinContent(ibin)
            L = 0.0
            if b > 0.0:
                L = s/math.sqrt(b)
                if L > ratio_High:
                    ratio_High = L
            ratio.SetBinContent(ibin, L)
        if ratio_High > 1.0:
            ratio_High = 1.0
        ratio.GetYaxis().SetRangeUser(ratio_Low, ratio_High*1.2)
        ratio.GetYaxis().SetTitle("S/#sqrt{B}")
        ratio.Draw("samehist")
    #if "xaxis_range" in extraoptions:
    #    ratio.GetXaxis().SetRangeUser(float(extraoptions["xaxis_range"][0]),float(extraoptions["xaxis_range"][1]))
    ratio.SetLineColor(1)
    ratio.SetLineWidth(2)
    ratio.SetMarkerStyle(20)
    ratio.SetMarkerColor(1)
    ratio.SetFillColorAlpha(1, 0)
    ratio.GetXaxis().SetTitleOffset(0.94)
    ratio.GetXaxis().SetTitleSize(0.18)
    ratio.GetXaxis().SetLabelSize(0.12)
    ratio.GetXaxis().SetLabelOffset(0.013)
    ratio.GetYaxis().SetTitleOffset(0.40)
    ratio.GetYaxis().SetTitleSize(0.17)
    ratio.GetYaxis().SetLabelSize(0.13)
    ratio.GetYaxis().SetTickLength(0.01)
    ratio.GetYaxis().SetNdivisions(505)
    #if "xaxis_range" in extraoptions:
    #    ratio.GetXaxis().SetRangeUser(float(extraoptions["xaxis_range"][0]),float(extraoptions["xaxis_range"][1]))

    if "cutflow" in hist_name_:
        ratio.GetXaxis().SetTitle("")
    if "xaxis_label" in extraoptions and extraoptions["xaxis_label"] != None:
        x_title = extraoptions["xaxis_label"]
        ratio.GetXaxis().SetTitle(x_title)
    ratio.GetYaxis().CenterTitle()

    ##########draw ATLAS 
    pad1.cd()
   
    lumi_value = 139
    if "lumi_value" in extraoptions:
        lumi_value = extraoptions["lumi_value"]
    
    text1 = r.TLatex(leftMargin+0.03, 0.85, "ATLAS")
    text1.SetNDC()
    text1.SetTextFont(72)
    text1.SetTextSize(0.070)
    text1.Draw()

    text2 = r.TLatex(leftMargin+0.2, 0.85, "Internal   #sqrt{s}= 13 TeV   #int L dt = %d"%lumi_value+" fb^{-1}")
    text2.SetNDC()
    text2.SetTextFont(42)
    text2.SetTextSize(0.055)
    text2.Draw()


    outFile = dir_name_
    if output_name_:
        outFile = outFile + "/" +output_name_
    else:
        outFile = outFile + "/" + hist_name_

    #print("maxY = "+str(maxY))
    #Y maximum range
    stack.SetMaximum(maxY*1.6)

    #print everything into txt file
    text_file = open(outFile+"_linY.txt", "w")
    text_file.write("bin    |   x    ")
    for idx in range(len(bkg_legends_)):
        text_file.write(" | %21s"%bkg_legends_[idx])
    text_file.write(" | %21s"%("total B"))
    for idx in range(len(sig_legends_)):
        sig_legends_temp = sig_legends_[idx]
        if " x " in sig_legends_temp:
            sig_legends_temp = sig_legends_temp[0:sig_legends_temp.index(" x ")]
        text_file.write(" | %25s"%sig_legends_temp)
    if h1_data:
        text_file.write(" | data | data/mc")
    text_file.write("\n-------------")
    for idx in range(24*(len(bkg_legends_) + 1)+ 29*len(sig_legends_)):
        text_file.write("-")
    if h1_data:
        text_file.write("-------")
    text_file.write("\n")
    for ibin in range(0,h1_bkg[0].GetNbinsX()+1):
        text_file.write("%3d"%ibin+"   ")
        text_file.write(" | %6.3f"%ratio.GetBinCenter(ibin)+" ")
        for idx in range(len(bkg_legends_)):
            text_file.write(" | %7.3f "%h1_bkg[idx].GetBinContent(ibin)+"$\\pm$"+ " %7.3f"%h1_bkg[idx].GetBinError(ibin))
        text_file.write(" | %7.3f "%hist_b.GetBinContent(ibin)+"$\\pm$"+ " %7.3f"%hist_b.GetBinError(ibin))
        for idx in range(len(sig_legends_)):
            text_file.write(" | %9.3f "%(h1_sig[idx].GetBinContent(ibin)/sig_scale_)+"$\\pm$"+ " %9.3f"%(h1_sig[idx].GetBinError(ibin)/sig_scale_))
        if h1_data:
            text_file.write(" | %d"%h1_data.GetBinContent(ibin) +  " | %7.3f "%ratio.GetBinContent(ibin) +"$\\pm$"+ " %7.3f"%ratio.GetBinError(ibin))
        text_file.write("\n")
    text_file.close()
    os.system("cp "+outFile+"_linY.txt "+outFile+"_logY.txt")

    
    myC.SaveAs(outFile+"_linY.png")
    myC.SaveAs(outFile+"_linY.pdf")
    myC.SaveAs(outFile+"_linY.C")
    pad1.cd()
    stack.SetMaximum(maxY*8000.0)
    stack.SetMinimum(0.01)
    pad1.SetLogy()
    myC.SaveAs(outFile+"_logY.png")
    myC.SaveAs(outFile+"_logY.pdf")
    myC.SaveAs(outFile+"_logY.C")
    #save histogram and ratio to root file
    outFile_root = r.TFile(outFile+".root", "recreate")
    outFile_root.cd()
    for idx in range(len(bkg_legends_)):
        h1_bkg[idx].Write()
    for idx in range(len(sig_legends_)):
        h1_sig[idx].Write()
    if  h1_data:
        h1_data.Write()
        ratio.Write()
    #outFile_root.Write()
    outFile_root.Close()

def makeplot_all(
    sig_fnames_=None,
    bkg_fnames_=None,
    data_fname_=None,
    sig_legends_=None,
    bkg_legends_=None,
    sig_colors_=None,
    bkg_colors_=None,
    regions_name_=None,
    sig_scale_=1.0,
    dir_name_="plots",
    output_name_=None,
    extraoptions=None
    ):

    if regions_name_ == None or sig_fnames_ == None:
        print("nothing to plot....")
        return
    print("making all plots in the following regions: ")
    print(regions_name_)

    hist_names = []
    for region in regions_name_:
        tf = r.TFile(bkg_fnames_[0])
        for key in tf.GetListOfKeys():
            if region in key.GetName():
                hist_names.append(key.GetName())
    print("find the following histgrams to plot:")
    print(hist_names)
    for hist_name in hist_names:
        if "_v_" in hist_name:
            makeplot_single_2d(sig_fnames_=sig_fnames_, bkg_fnames_ = bkg_fnames_, data_fname_=data_fname_, sig_legends_=sig_legends_, bkg_legends_=bkg_legends_, sig_colors_=sig_colors_, bkg_colors_=bkg_colors_, hist_name_=hist_name, dir_name_=dir_name_, extraoptions=extraoptions)
        else:
            makeplot_single(sig_fnames_=sig_fnames_, bkg_fnames_ = bkg_fnames_, data_fname_=data_fname_, sig_legends_=sig_legends_, bkg_legends_=bkg_legends_, sig_colors_=sig_colors_, bkg_colors_=bkg_colors_, hist_name_=hist_name, sig_scale_=sig_scale_, dir_name_=dir_name_, extraoptions=extraoptions)

def makeplot_cutOptimize(
    sig_fnames_=None,
    bkg_fnames_=None,
    sig_legends_=None,
    bkg_legends_=None,
    sig_colors_=None,
    bkg_colors_=None,
    hist_name_=None,
    x_low_=None,
    x_high_=None,
    sig_scale_=1.0,
    bkg_scale_=1.0,
    dir_name_="plots",
    optimizeLimit=False,
    output_name_=None,
    extraoptions=None
    ):

    if sig_fnames_ == None or bkg_fnames_ == None or hist_name_ == None or sig_legends_ == None or bkg_legends_ == None:
        print("nothing to plot....")
        return
   
    s_color = [632, 617, 839, 800, 1]
    b_color = [920, 2007, 2005, 2003, 2001, 2011]
    if sig_colors_:
        s_color = sig_colors_
    if bkg_colors_:
        b_color = bkg_colors_
    
    tfs_sig = {}
    h1_sig = []
    h1_sig_L = []
    h1_sig_R = []
    for idx in range(len(sig_fnames_)): 
        fn = sig_fnames_[idx]
        n = os.path.basename(fn.replace(".root", ""))
        tfs_sig[n] = r.TFile(fn)
        h1 = tfs_sig[n].Get(hist_name_)
        h1.SetName(hist_name_+"_sig_"+str(idx))
        removeLowHigh(h1, x_low_, x_high_)
        h1.Scale(sig_scale_)
        h1.SetLineWidth(2)
        h1.SetLineColor(s_color[idx])
        h1_sig.append(h1)
        h1_sig_L.append(h1.Clone(hist_name_+"_sigL_"+str(idx)))
        h1_sig_R.append(h1.Clone(hist_name_+"_sigR_"+str(idx)))

    tfs_bkg = {}
    h1_bkg = []
    h1_bkg_L = []
    h1_bkg_R = []
    for idx in range(len(bkg_fnames_)): 
        fn = bkg_fnames_[idx]
        n = os.path.basename(fn.replace(".root", ""))
        tfs_bkg[n] = r.TFile(fn)
        h1 = tfs_bkg[n].Get(hist_name_)
        h1.SetName(hist_name_+"_bkg_"+str(idx))
        removeLowHigh(h1, x_low_, x_high_)
        h1.Scale(bkg_scale_)
        h1.SetLineWidth(2)
        h1.SetLineColor(b_color[idx])
        h1.SetFillColorAlpha(b_color[idx], 1)
        h1_bkg.append(h1)
        h1_bkg_L.append(h1.Clone(hist_name_+"_bkgL_"+str(idx)))
        h1_bkg_R.append(h1.Clone(hist_name_+"_bkgR_"+str(idx)))
   
    if "remove_underflow" in extraoptions:
        if extraoptions["remove_underflow"]:
            remove_underflow(h1_sig)
            remove_underflow(h1_bkg)
    if "remove_overflow" in extraoptions:
        if extraoptions["remove_overflow"]:
            remove_overflow(h1_sig)
            remove_overflow(h1_bkg)

    myC = r.TCanvas("myC","myC", 600, 600)
    myC.SetTicky(1)


    pad0 = r.TPad("pad0","pad0", 0.05, 0.80,0.95, 0.99)
    pad0.SetBottomMargin(0)
    pad0.SetRightMargin( rightMargin )
    pad0.SetLeftMargin( leftMargin )

    pad1 = r.TPad("pad1","pad1", 0.05, 0.5,0.95, 0.80)
    pad1.SetBottomMargin(0)
    pad1.SetRightMargin( rightMargin )
    pad1.SetLeftMargin( leftMargin )

    pad2 = r.TPad("pad1","pad1", 0.05, 0.2,0.95, 0.50)
    pad2.SetBottomMargin(0)
    pad2.SetTopMargin(0.008)
    pad2.SetTopMargin(0.008)
    pad2.SetRightMargin( rightMargin )
    pad2.SetLeftMargin( leftMargin )

    pad3 = r.TPad("pad3","pad3", 0.05, 0.0, 0.95, 0.2)
    pad3.SetBottomMargin(0.3)
    pad3.SetTopMargin(0.008)
    pad3.SetRightMargin( rightMargin )
    pad3.SetLeftMargin( leftMargin )

    pad3.Draw()
    pad2.Draw()
    pad1.Draw()
    pad0.Draw()

    pad1.cd()


    hist_noCut_s = h1_sig[0].Clone("hist_noCut_s")
    hist_noCut_b = h1_bkg[0].Clone("hist_noCut_b")
    for idx in range(len(h1_bkg)):
        if idx > 0:
            hist_noCut_b.Add(h1_bkg[idx])
    for idx in range(len(h1_sig)):
        if idx > 0:
            hist_noCut_s.Add(h1_sig[idx])
    nbins = hist_noCut_s.GetNbinsX()
    print("nbins: "+str(nbins))
    print("nS before cut: "+ str(hist_noCut_s.Integral()/sig_scale_))
    print("nB before cut: "+ str(hist_noCut_b.Integral()))
    ratio = hist_noCut_s.Clone("ratio")
    hist_b = hist_noCut_b.Clone("hist_b")
    hist_s = hist_noCut_b.Clone("hist_s")

    maxY1_L = 0.0
    maxY1_R = 0.0
    maxY2 = 0.0
    minY2 = 999999.9
    bestCut = 0 
    s1_best = 0.0
    b1_best = 0.0
    s2_best = 0.0
    b2_best = 0.0
    L1_best = 0.0
    L2_best = 0.0
    L_best = 0.0
    mu1_best = 999999.9
    mu2_best = 999999.9
    mu_best = 999999.9
    for ibin in range(0, nbins+1):
        s1 = hist_noCut_s.Integral(0, ibin)/sig_scale_ 
        b1 = hist_noCut_b.Integral(0, ibin)
        s2 = hist_noCut_s.Integral(ibin+1, nbins+1)/sig_scale_
        b2 = hist_noCut_b.Integral(ibin+1, nbins+1)
        s = s1+s2
        b = b1+b2
        for idx in range(len(h1_sig)):
            s1_this = h1_sig[idx].Integral(0, ibin)
            s2_this = h1_sig[idx].Integral(ibin+1, nbins+1)
            h1_sig_L[idx].SetBinContent(ibin, s1_this)
            h1_sig_R[idx].SetBinContent(ibin, s2_this)
        for idx in range(len(h1_bkg)):
            b1_this = h1_bkg[idx].Integral(0, ibin)
            b2_this = h1_bkg[idx].Integral(ibin+1, nbins+1)
            h1_bkg_L[idx].SetBinContent(ibin, b1_this)
            h1_bkg_R[idx].SetBinContent(ibin, b2_this)
        L1 = significance(s1, b1) 
        L2 = significance(s2, b2)
        L = math.sqrt(L1*L1+L2*L2)
        mu1 = upperLimit(s1, b1) 
        mu2 = upperLimit(s2, b2)
        mu = 1./math.sqrt(1./(mu1*mu1)+1./(mu2*mu2))
        if s1 > maxY1_L:
            maxY1_L = s1
        if b1 > maxY1_L:
            maxY1_L = b1
        if s2 > maxY1_R:
            maxY1_R = s2
        if b2 > maxY1_R:
            maxY1_R = b2
        if L > L_best and b2 > 0.8 and b1 > 0.8 and (not optimizeLimit):
            maxY2 = L
            bestCut = ibin
            s1_best = s1
            s2_best = s2
            b1_best = b1
            b2_best = b2
            L1_best = L1
            L2_best = L2
            L_best = L
        if mu < mu_best  and b2 > 0.8 and b1 > 0.8 and optimizeLimit:
            minY2 = mu
            bestCut = ibin
            s1_best = s1
            s2_best = s2
            b1_best = b1
            b2_best = b2
            mu1_best = mu1
            mu2_best = mu2
            mu_best = mu
        
        if optimizeLimit:
            ratio.SetBinContent(ibin, mu)
        else:
            ratio.SetBinContent(ibin, L)
        hist_b.SetBinContent(ibin, b2)
        hist_s.SetBinContent(ibin, s2)
    
    x_title = "cut on "+hist_noCut_s.GetXaxis().GetTitle()
    if "xaxis_label" in extraoptions and extraoptions["xaxis_label"] != None:
        x_title = extraoptions["xaxis_label"]

    stack_L = r.THStack("stack_L", "stack_L")
    for idx in range(len(h1_bkg_L)):
        stack_L.Add(h1_bkg_L[idx])
    stack_L.SetTitle("")
    stack_L.Draw("hist")
    for idx in range(len(h1_sig_L)):
        h1_sig_L[idx].Draw("samehist")
    stack_L.GetYaxis().SetTitle("Events (left)")
    stack_L.GetYaxis().SetTitleOffset(0.6)
    stack_L.GetYaxis().SetTitleSize(0.11)
    stack_L.GetYaxis().SetLabelSize(0.09)
    stack_L.GetYaxis().CenterTitle()
    x_low_edge = hist_b.GetBinCenter(1)-0.5*hist_b.GetBinWidth(1)
    x_high_edge = hist_b.GetBinCenter(nbins)+0.5*hist_b.GetBinWidth(nbins)
    if x_low_ != None and x_high_ != None:
        stack_L.GetXaxis().SetRangeUser(x_low_, x_high_)
    elif x_high_ != None:
        stack_L.GetXaxis().SetRangeUser(x_low_edge, x_high_)
    elif x_low_ != None:
        stack_L.GetXaxis().SetRangeUser(x_low_, x_high_edge)
        

    pad2.cd()

    stack_R = r.THStack("stack_R", "stack_R")
    for idx in range(len(h1_bkg_R)):
        stack_R.Add(h1_bkg_R[idx])
    stack_R.SetTitle("")
    stack_R.Draw("hist")
    for idx in range(len(h1_sig_R)):
        h1_sig_R[idx].Draw("samehist")
    stack_R.GetYaxis().SetTitle("Events (right)")
    stack_R.GetYaxis().SetTitleOffset(0.6)
    stack_R.GetYaxis().SetTitleSize(0.11)
    stack_R.GetYaxis().SetLabelSize(0.09)
    stack_R.GetYaxis().CenterTitle()

    if x_low_ != None and x_high_ != None:
        stack_R.GetXaxis().SetRangeUser(x_low_, x_high_)
    elif x_high_ != None:
        stack_R.GetXaxis().SetRangeUser(x_low_edge, x_high_)
    elif x_low_ != None:
        stack_R.GetXaxis().SetRangeUser(x_low_, x_high_edge)

    pad3.cd()
    pad3.SetGridy(1)
   
    if optimizeLimit:
        maxY2 = 2.0*minY2
        ratio.GetYaxis().SetRangeUser(minY2*0.8, maxY2)
        ratio.GetYaxis().SetTitle("#mu_{up}")
    else:
        minY2 = 0.0
        ratio.GetYaxis().SetRangeUser(minY2, 1.2*maxY2)
        ratio.GetYaxis().SetTitle("L")
    ratio.GetYaxis().CenterTitle()
    ratio.Draw("samehist")
    ratio.SetLineColor(1)
    ratio.SetLineWidth(2)
    ratio.SetMarkerStyle(20)
    ratio.SetMarkerColor(1)
    ratio.SetFillColorAlpha(1, 0)
    ratio.GetXaxis().SetTitleOffset(0.7)
    ratio.GetXaxis().SetTitleSize(0.18)
    ratio.GetXaxis().SetLabelSize(0.12)
    ratio.GetXaxis().SetLabelOffset(0.013)
    ratio.GetYaxis().SetTitleOffset(0.40)
    ratio.GetYaxis().SetTitleSize(0.17)
    ratio.GetYaxis().SetLabelSize(0.13)
    ratio.GetYaxis().SetTickLength(0.01)
    ratio.GetYaxis().SetNdivisions(505)
    ratio.GetXaxis().SetTitle(x_title)

    if x_low_ != None and x_high_ != None:
        ratio.GetXaxis().SetRangeUser(x_low_, x_high_)
    elif x_high_ != None:
        ratio.GetXaxis().SetRangeUser(x_low_edge, x_high_)
    elif x_low_ != None:
        ratio.GetXaxis().SetRangeUser(x_low_, x_high_edge)
    ##########draw ATLAS 

    pad0.cd()
    lumi_value = 137
    if "lumi_value" in extraoptions:
        lumi_value = extraoptions["lumi_value"]
    #ATLASLabel(leftMargin+0.1, 0.85, lumi_value)
 
    text1 = r.TLatex(leftMargin, 0.75, "ATLAS")
    text1.SetNDC()
    text1.SetTextFont(72)
    text1.SetTextSize(0.20)
    text1.Draw()

    text2 = r.TLatex(leftMargin+0.14, 0.75, "Internal   #sqrt{s}= 13 TeV   #int L dt = %d"%lumi_value+" fb^{-1}")
    text2.SetNDC()
    text2.SetTextFont(42)
    text2.SetTextSize(0.155)
    text2.Draw()
    leg = r.TLegend(leftMargin, 0.32, 0.97, 0.73)
    leg.SetNColumns(4)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.2)
    for idx in range(len(h1_bkg)):
        leg.AddEntry(h1_bkg[idx], bkg_legends_[idx], "F")
    for idx in range(len(h1_sig)):
        leg.AddEntry(h1_sig[idx], sig_legends_[idx], "L")
    leg.Draw()


    ##print out best cut and s, b, L
    c_best = ratio.GetBinCenter(bestCut)
    print("best cut: %6.3f"%c_best)
    print("L at best cut: %7.3f"%L_best)

    tex4 = r.TLatex(leftMargin, 0.17, "best cut = %.4f"%c_best+", L1 = %.2f"%L1_best+", L2 = %.2f"%L2_best+", L = %.2f"%L_best)
    if optimizeLimit:
        tex4 = r.TLatex(leftMargin, 0.17, "best cut = %.4f"%c_best+", #mu1 = %.f"%mu1_best+", #mu2 = %.1f"%mu2_best+", #mu = %.1f"%mu_best)

    tex4.SetNDC()
    tex4.SetTextFont(42)
    tex4.SetTextSize(0.155)
    tex4.SetLineWidth(2)
    tex4.Draw()

    tex5 = r.TLatex(leftMargin, 0.02, "s1 = %.2f"%s1_best+", b1 = %.2f"%b1_best+", s2 = %.2f"%s2_best+", b2 = %.2f"%b2_best)
    tex5.SetNDC()
    tex5.SetTextFont(42)
    tex5.SetTextSize(0.155)
    tex5.SetLineWidth(2)
    tex5.Draw()

    #draw vertical line indcating best cut

    pad1.cd()
    line1 = r.TLine(c_best,0.0,c_best,1.0*maxY1_L)
    line1.SetLineStyle(7)
    line1.Draw("same")
    pad2.cd()
    line1_R = r.TLine(c_best,0.0,c_best,1.0*maxY1_R)
    line1_R.SetLineStyle(7)
    line1_R.Draw("same")
    pad3.cd()
    line2 = r.TLine(c_best,minY2,c_best,1.2*maxY2)
    line2.SetLineStyle(7)
    line2.Draw("same")

    outFile = dir_name_
    if output_name_:
        outFile = outFile + "/" +output_name_
    else:
        if optimizeLimit:
            outFile = outFile + "/" + hist_name_+"_limit"
        else:
            outFile = outFile + "/" + hist_name_+"_significance"

    stack_L.SetMaximum(maxY1_L*1.1)
    stack_R.SetMaximum(maxY1_L*1.1)

    #print everything into txt file
    text_file = open(outFile+"_optimize_linY.txt", "w")
    text_file.write("bin    |   x    ")
    for idx in range(len(bkg_legends_)):
        text_file.write(" | %21s"%bkg_legends_[idx])
    text_file.write(" | %21s"%("total B"))
    for idx in range(len(sig_legends_)):
        text_file.write(" | %25s"%sig_legends_[idx])
    if optimizeLimit:
        text_file.write(" | %21s"%("mu"))
    else:
        text_file.write(" | %21s"%("L"))
    text_file.write("\n-------------")
    for idx in range(24*(len(bkg_legends_) + 2)+ 29*len(sig_legends_)):
        text_file.write("-")
    text_file.write("\n")
    for ibin in range(0,h1_sig_R[0].GetNbinsX()+1):
        text_file.write("%3d"%ibin+"   ")
        text_file.write(" | %8.5f"%ratio.GetBinCenter(ibin)+" ")
        for idx in range(len(bkg_legends_)):
            text_file.write(" | %7.3f "%h1_bkg_R[idx].GetBinContent(ibin)+"$\\pm$"+ " %7.3f"%h1_bkg_R[idx].GetBinError(ibin))
        text_file.write(" | %7.3f "%hist_b.GetBinContent(ibin)+"$\\pm$"+ " %7.3f"%hist_b.GetBinError(ibin))
        for idx in range(len(sig_legends_)):
            text_file.write(" | %9.3f "%h1_sig_R[idx].GetBinContent(ibin)+"$\\pm$"+ " %9.3f"%h1_sig_R[idx].GetBinError(ibin))
        text_file.write(" | %9.3f "%ratio.GetBinContent(ibin))
        text_file.write("\n")
    text_file.close()
    os.system("cp "+outFile+"_optimize_linY.txt "+outFile+"_optimize_logY.txt")

    myC.SaveAs(outFile+"_optimize_linY.png")
    myC.SaveAs(outFile+"_optimize_linY.pdf")
    myC.SaveAs(outFile+"_optimize_linY.C")
    pad1.cd()
    stack_L.SetMinimum(0.01)
    pad1.SetLogy()
    pad2.cd()
    stack_R.SetMinimum(0.01)
    pad2.SetLogy()
    myC.SaveAs(outFile+"_optimize_logY.png")
    myC.SaveAs(outFile+"_optimize_logY.pdf")
    myC.SaveAs(outFile+"_optimize_logY.C")


def makeplot_ROC(
    sig_fnames_=None,
    bkg_fnames_=None,
    sig_legends_=None,
    bkg_legends_=None,
    sig_colors_=None,
    bkg_colors_=None,
    hist_name_=None,
    sig_scale_=1.0,
    bkg_scale_=1.0,
    dir_name_="plots",
    output_name_=None,
    right_hand_=False,
    extraoptions=None
    ):

    if sig_fnames_ == None or bkg_fnames_ == None or hist_name_ == None or sig_legends_ == None or bkg_legends_ == None:
        print("nothing to plot....")
        return
   
    s_color = [632, 617, 839, 800, 1]
    b_color = [920, 2007, 2005, 2003, 2001, 2011]
    if sig_colors_:
        s_color = sig_colors_
    if bkg_colors_:
        b_color = bkg_colors_
    
    tfs_sig = {}
    h1_sig = []
    for idx in range(len(sig_fnames_)): 
        fn = sig_fnames_[idx]
        n = os.path.basename(fn.replace(".root", ""))
        tfs_sig[n] = r.TFile(fn)
        h1 = tfs_sig[n].Get(hist_name_)
        h1.SetName(hist_name_+"_sig_"+str(idx))
        h1.Scale(sig_scale_)
        h1.SetLineWidth(2)
        h1.SetLineColor(s_color[idx])
        h1_sig.append(h1)

    tfs_bkg = {}
    h1_bkg = []
    for idx in range(len(bkg_fnames_)): 
        fn = bkg_fnames_[idx]
        n = os.path.basename(fn.replace(".root", ""))
        tfs_bkg[n] = r.TFile(fn)
        h1 = tfs_bkg[n].Get(hist_name_)
        h1.SetName(hist_name_+"_bkg_"+str(idx))
        h1.Scale(bkg_scale_)
        h1.SetLineWidth(2)
        h1.SetLineColor(b_color[idx])
        h1.SetFillColorAlpha(b_color[idx], 1)
        h1_bkg.append(h1)
   
    if "remove_underflow" in extraoptions:
        if extraoptions["remove_underflow"]:
            remove_underflow(h1_sig)
            remove_underflow(h1_bkg)
    if "remove_overflow" in extraoptions:
        if extraoptions["remove_overflow"]:
            remove_overflow(h1_sig)
            remove_overflow(h1_bkg)

    hist_all_b = h1_bkg[0].Clone("hist_all_b")
    for idx in range(len(h1_bkg)):
        if idx > 0:
            hist_all_b.Add(h1_bkg[idx])
    hist_all_s = h1_sig[0].Clone("hist_all_s")
    for idx in range(len(h1_sig)):
        if idx > 0:
            hist_all_s.Add(h1_sig[idx])

    myC = r.TCanvas("myC","myC", 600, 600)
    myC.SetTicky(1)
    myC.SetTickx(1)

    nbins = h1_sig[0].GetNbinsX()+1

    eff_sig = np.zeros(nbins+1)
    eff_bkg = np.zeros((len(h1_bkg)+1, nbins+1))
    AUC = np.zeros(len(h1_bkg)+1)

    ntotal_sig = hist_all_s.Integral(0, nbins)
    ntotal_bkg = np.zeros(len(h1_bkg)+1)
    for idx in range(len(h1_bkg)):
        ntotal_bkg[idx] = h1_bkg[idx].Integral(0, nbins)
    ntotal_bkg[-1] = hist_all_b.Integral(0, nbins)

    for idx in range(0, nbins+1):
        npass_sig = hist_all_s.Integral(idx, nbins) 
        if ntotal_sig < 1e-8:
            eff_sig[idx] = 1.0
        else:
            eff_sig[idx] = npass_sig/ntotal_sig
        
        for ib in range(len(h1_bkg)):
            npass_bkg = h1_bkg[ib].Integral(idx, nbins)
            if ntotal_bkg[ib] < 1e-8:
                eff_bkg[ib][idx]  = 1.0
            else:
                eff_bkg[ib][idx] =  npass_bkg/ntotal_bkg[ib]
        npass_bkg = hist_all_b.Integral(idx, nbins)
        if ntotal_bkg[-1] < 1e-8:
            eff_bkg[-1][idx] = 1.0
        else:
            eff_bkg[-1][idx] = npass_bkg/ntotal_bkg[-1]

    grs = []
    for idx in range(len(h1_bkg)+1):
        gr = r.TGraph(nbins+1, eff_bkg[idx], eff_sig)
        gr.SetLineColor(b_color[idx])
        gr.SetLineWidth(2)
        grs.append(gr)
        AUC[idx] = 0.5 + gr.Integral()
    grs[0].Draw("AL")

    grs[0].GetYaxis().SetTitle("Signal Efficiency")
    grs[0].GetYaxis().SetTitleOffset(0.85)
    grs[0].GetYaxis().SetTitleSize(0.07)
    grs[0].GetYaxis().SetLabelSize(0.045)

    grs[0].GetXaxis().SetTitle("Background Efficiency")
    grs[0].GetXaxis().SetTitleOffset(0.94)
    grs[0].GetXaxis().SetTitleSize(0.06)
    grs[0].GetXaxis().SetLabelSize(0.045)
    grs[0].GetXaxis().SetLabelOffset(0.013)
    grs[0].SetMaximum(1.20)
    grs[0].SetMinimum(0.0)
    grs[0].GetXaxis().SetLimits(0.0,1.0)

    for idx in range(1, len(h1_bkg)+1):
        grs[idx].Draw("Lsame")

    leg = r.TLegend(0.3, 0.20, 0.92, 0.5)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.045)
    for idx in range(len(h1_bkg)):
        leg.AddEntry(grs[idx], bkg_legends_[idx]+", AUC = %.2f"%AUC[idx], "l")
    leg.AddEntry(grs[-1], "Total bkg, AUC = %.2f"%AUC[-1], "l")
    leg.Draw()
    
    #draw line of y=x
    f2 = r.TF1("f2", "0.0+1.0*x", 0.0, 1.0)
    f2.SetLineColor(r.kBlack)
    f2.SetLineStyle(9)
    f2.SetLineWidth(1)
    #f2.Draw("same")

    ##########draw ATLAS 
    lumi_value = 137
    if "lumi_value" in extraoptions:
        lumi_value = extraoptions["lumi_value"]
    #ATLASLabel(leftMargin+0.1, 0.85, lumi_value)
 
    text1 = r.TLatex(leftMargin+0.055, 0.85, "ATLAS")
    text1.SetNDC()
    text1.SetTextFont(72)
    text1.SetTextSize(0.05)
    text1.Draw()

    text2 = r.TLatex(leftMargin+0.225, 0.85, "Internal  #sqrt{s}= 13 TeV #int L dt = %d"%lumi_value+" fb^{-1}")
    text2.SetNDC()
    text2.SetTextFont(42)
    text2.SetTextSize(0.04)
    text2.Draw()

    outFile = dir_name_
    if output_name_:
        outFile = outFile + "/" +output_name_
    else:
        outFile = outFile + "/" + hist_name_


    #print everything into txt file
    text_file = open(outFile+"_ROC_linY.txt", "w")
    text_file.write("cut    ")
    for idx in range(len(bkg_legends_)):
        text_file.write(" | %8s"%bkg_legends_[idx])
    text_file.write(" | %8s"%("total B"))
    text_file.write(" | signal")
    text_file.write("\n-------------")
    for idx in range(10*(len(bkg_legends_) + 1)+ 10):
        text_file.write("-")
    text_file.write("\n")
    for ibin in range(0,nbins+1):
        text_file.write("%6.4f"%h1_sig[0].GetBinCenter(ibin)+" ")
        for idx in range(len(bkg_legends_)+1):
            text_file.write(" | %7.4f "%eff_bkg[idx][ibin])
        text_file.write(" | %7.4f "%eff_sig[ibin])
        text_file.write("\n")
    text_file.close()
    os.system("cp "+outFile+"_ROC_linY.txt "+outFile+"_ROC_logY.txt")

    myC.SaveAs(outFile+"_ROC_linY.png")
    myC.SaveAs(outFile+"_ROC_linY.pdf")
    myC.SaveAs(outFile+"_ROC_linY.C")

    grs[0].SetMaximum(5.)
    grs[0].SetMinimum(0.001)
    grs[0].GetXaxis().SetLimits(0.001,1.0)
    myC.SetLogy()
    myC.SetLogx()
    myC.SaveAs(outFile+"_ROC_logY.png")
    myC.SaveAs(outFile+"_ROC_logY.pdf")
    myC.SaveAs(outFile+"_ROC_logY.C")

