import ROOT as rt

import sys
import os


def GetKeyNames( self, dir = "" ):
        self.cd(dir)
        return [key.GetName() for key in rt.gDirectory.GetListOfKeys()]
def GetClassNames( self, dir = "" ):
        self.cd(dir)
        return [key.GetClassName() for key in rt.gDirectory.GetListOfKeys()]

rt.TFile.GetKeyNames = GetKeyNames
rt.TFile.GetClassNames = GetClassNames

xsec_file = open("xsec.tsv", "r")


dict_xsec = {}
lines_xsec = xsec_file.readlines()
for line in lines_xsec:
    line_arr = line.split()
    dict_xsec[line_arr[0]] = [float(line_arr[2]), float(line_arr[3]), float(line_arr[4])] #xsec, gen_filt, k_factor


def get_normalization(dsid, sum_weights):
    xsec_all = dict_xsec[str(dsid)]
    lumi = 1.0 # /fb
    xsec = xsec_all[0] # pb
    gen_filt = xsec_all[1]
    k_factor = xsec_all[2]

    print("get normalization for "+str(dsid)+" : "+ str(xsec)+ "(xsec), "+str(gen_filt)+"(gen_filt), "+str(k_factor)+"(k_fac), "+str(sum_weights)+"(sum_weights)")

    return lumi*xsec*1000.0*k_factor*gen_filt/sum_weights


if __name__ == '__main__':
    inputFile = "/global/projecta/projectdirs/atlas/cecily/Ntuple_h026_0530/nti_all.root" 
    if len(sys.argv) > 1:
        inputFile = sys.argv[1]
    outFile = "/global/projecta/projectdirs/atlas/zhicaiz/HH/samples/Ntuple_h026_0630/nti_all_skim.root"
    if len(sys.argv) > 2:
        outFile = sys.argv[2]

    os.system("rm "+outFile)

    cut = "nboostedJets>1 && log(boostedJets_Xbb2020v3_Higgs[0]/(0.75*boostedJets_Xbb2020v3_QCD + 0.25*boostedJets_Xbb2020v3_Top[0])) > -2.0 && log(boostedJets_Xbb2020v3_Higgs[1]/(0.75*boostedJets_Xbb2020v3_QCD[1] + 0.25*boostedJets_Xbb2020v3_Top[1])) > -2.0"

    print("input: "+inputFile)
    print("output: "+outFile)
    print("cut: "+cut)

    inFile = rt.TFile(inputFile, "READ")

    metadata = inFile.Get("MetaData_EventCount_XhhMiniNtuple")
    
    sum_weights = metadata.GetBinContent(3)
    
    inTree = inFile.Get("XhhMiniNtuple")

    if inTree.GetEntries() < 1:
        sys.exit(0)

    inTree.GetEntry(0)
    mcChannelNumber = inTree.mcChannelNumber

    #print(mcChannelNumber)
    if mcChannelNumber >= 361020 and mcChannelNumber <=361032:
        sum_weights = metadata.GetBinContent(1)

    normalization = get_normalization(mcChannelNumber, sum_weights)
    print("normalization = "+str(normalization))

    rt.gROOT.ProcessLine("struct MyStruct{float event_weight;};")
    from ROOT import MyStruct


    keyList = inFile.GetKeyNames()
    classList = inFile.GetClassNames()
    
    outFile = rt.TFile(outFile, "RECREATE")
    outFile.cd()

    outputTree = inTree.CopyTree(cut)

    my_s = MyStruct()
    rt.addressof(my_s, "event_weight")
    b_weight = outputTree.Branch("event_weight", rt.addressof(my_s, "event_weight"), "event_weight/F")

    nEntries = outputTree.GetEntries()
    for i in range(nEntries):
        outputTree.GetEntry(i)
        my_s.event_weight = outputTree.mcEventWeight * outputTree.weight_pileup * normalization
        b_weight.Fill()
    print("events before cut: "+str(inTree.GetEntries()))
    outputTree.Write()
    print("events after cut: "+str(outputTree.GetEntries()))

    for j in range(0, len(keyList)):
        if classList[j] == "TH1F" or classList[j] == "TH1D":
            inFile.cd()
            histThis = inFile.Get(keyList[j])
            outFile.cd()
            histThis_out = histThis.Clone()
            histThis_out.Write()
    outFile.Close()


