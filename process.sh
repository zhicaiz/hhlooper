
TAG=$1

mkdir -p hists/${TAG}/
rm -rf hists/${TAG}/*

inputBase=/global/projecta/projectdirs/atlas/zhicaiz/HH4b/samples_skim/


for year in 16a 16d 16e
do
 (set -x ;./HHLooper "${inputBase}/HH4b_ggF_Pythia/*600463*MC${year}*MiniNTuple/*.root" HH.root ${TAG} 0 >&1) &
 for ijet in {00..12}
 do
     (set -x ;./HHLooper "${inputBase}/dijet/*3647${ijet}*MC${year}*MiniNTuple/*.root" dijet_JZ${ijet}.root ${TAG} 0 >&1) &
 done
 (set -x ;./HHLooper "${inputBase}/ttH/*346343*MC${year}*MiniNTuple/*.root" ttH_allhad.root ${TAG} 0 >&1) &
 (set -x ;./HHLooper "${inputBase}/ttH/*346344*MC${year}*MiniNTuple/*.root" ttH_semilep.root ${TAG} 0 >&1) &
 (set -x ;./HHLooper "${inputBase}/ttH/*346345*MC${year}*MiniNTuple/*.root" ttH_dilep.root ${TAG} 0 >&1) &
 (set -x ;./HHLooper "${inputBase}/ttbar/*MC${year}*MiniNTuple/*.root" ttbar.root ${TAG} 0 >&1) &
 (set -x ;./HHLooper "${inputBase}/VH/*346639*MC${year}*MiniNTuple/*.root" WmH.root ${TAG} 0 >&1) &
 (set -x ;./HHLooper "${inputBase}/VH/*346640*MC${year}*MiniNTuple/*.root" WpH.root ${TAG} 0 >&1) &
 (set -x ;./HHLooper "${inputBase}/ggH/*345342*MC${year}*MiniNTuple/*.root" ggH.root ${TAG} 0 >&1) &
 (set -x ;./HHLooper "${inputBase}/VH/*346641*MC${year}*MiniNTuple/*.root" ZH.root ${TAG} 0 >&1) &
 (set -x ;./HHLooper "${inputBase}/VBFH/*345949*MC${year}*MiniNTuple/*.root" VBFH.root ${TAG} 0 >&1) &
done

(set -x ;./HHLooper "${inputBase}/ggH/*345342*all_test*MiniNTuple/*.root" ggH.root ${TAG} 0 >&1) &

sleep 1
wait

for year in 2016 2017 2018
do
    hadd -k -f hists/${TAG}/${year}/WH.root hists/${TAG}/${year}/WmH.root hists/${TAG}/${year}/WpH.root
    hadd -k -f hists/${TAG}/${year}/VH.root hists/${TAG}/${year}/WH.root hists/${TAG}/${year}/ZH.root
    hadd -k -f hists/${TAG}/${year}/ttH.root hists/${TAG}/${year}/ttH_*.root
    hadd -k -f hists/${TAG}/${year}/dijet.root hists/${TAG}/${year}/dijet_*.root
done

mkdir -p hists/${TAG}/all/

for proc in HH ttH VH ttH VBFH singleH dijet ttbar 
do
    hadd -k -f hists/${TAG}/all/${proc}.root hists/${TAG}/2016/${proc}.root hists/${TAG}/2017/${proc}.root hists/${TAG}/2018/${proc}.root
done

hadd -k -f hists/${TAG}/all/singleH.root hists/${TAG}/all/ttH.root hists/${TAG}/all/VH.root hists/${TAG}/all/VBFH.root hists/${TAG}/all/ggH.root


