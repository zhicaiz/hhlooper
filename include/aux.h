#ifndef aux_h
#define aux_h

#include <Math/Vector4D.h>

template<typename T>
using RVec = ROOT::VecOps::RVec<T>;

std::vector<float> combineXbb(const std::vector<float>& xbb_Higgs, const std::vector<float>& xbb_QCD, const std::vector<float>& xbb_Top) 
{
    std::vector<float> XbbCombined(xbb_Higgs.size());
    if(xbb_Higgs.size() < 1) return XbbCombined;
    float ftop = 0.25;
    for (unsigned int i=0; i < XbbCombined.size(); i++) XbbCombined[i] = std::log(xbb_Higgs[i]/((1-ftop)*xbb_QCD[i] + ftop*xbb_Top[i]));

    return XbbCombined;
};


std::vector<int> sortObjects(const std::vector<float>& var) 
{
    std::vector<int> sortIdx(var.size());
    if(var.size() < 1) return sortIdx;
    for (unsigned int i=0; i < sortIdx.size(); i++) sortIdx[i] = i;

    std::sort(sortIdx.begin(), sortIdx.end(),
            [=](const int a, const int b) {
              return var[a] > var[b];
            });
    return sortIdx;
};

float leadingVar(const std::vector<float>& var, const std::vector<int>& idx)
{
    if(var.size() < 1) return -999.;
    if(var.size() < idx[0]+1) return -999.;
    return var[idx[0]];
};

float subleadingVar(const std::vector<float>& var, const std::vector<int>& idx)
{
    if(var.size() < 1) return -999.;
    if(var.size() < idx[1]+1) return -999.;
    return var[idx[1]];
};

template <typename T> bool mgg_Hpeak(T mgg){ return mgg > 123. && mgg < 127.; };
template <typename T> bool mgg_Hsideband(T mgg){ return mgg <= 123. || mgg >= 127.; };


// calculate the invariant mass of the first two particles in a list
// parameter index: filled with 0 or 1, only take elements with index == 1
template <typename T>
T InvariantMassTwoBody(const RVec<T>& pt, const RVec<T>& eta, const RVec<T>& phi, const RVec<T>& mass, const RVec<int>& index)
{
   if(index.size() < 2) return -999;
   int idx1 = -1, idx2 = -1;
   for(int i=0; i<index.size(); i++){
    if(index[i] == 0) continue;
    if(idx1 == -1) {
        idx1 = i;
        continue;
    }
    if(idx2 == -1) idx2 = i;
   }

   if(idx2 == -1) return -999;

   ROOT::Math::PtEtaPhiMVector p1(pt[idx1], eta[idx1], phi[idx1], mass[idx1]);
   ROOT::Math::PtEtaPhiMVector p2(pt[idx2], eta[idx2], phi[idx2], mass[idx2]);
   return (p1 + p2).mass();
};


#endif
