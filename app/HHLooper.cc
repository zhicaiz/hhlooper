//C++ INCLUDES
#include <sys/stat.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <cstdlib>

//ROOT
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TROOT.h>
#include <TRandom3.h>

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include "ROOT/RDF/RInterface.hxx"

//LOCAL
#include "aux.h"

using namespace ROOT::VecOps;
using RNode = ROOT::RDF::RNode;


using namespace std;

double lumi = 138.96516;
TRandom3* r_nominal = new TRandom3(0);
const float pi = 3.141592653;


struct histogram_type{
   TString savename;
   TString title;
   int nbins;
   double xlow;
   double xhigh;
   std::string varname;
};

struct cut_type{
   TString cutname;
   std::string weight;
   RNode node;
};


int main ( int argc, char* argv[])
{

if(argc < 4) 
{
 cout<<"ERROR  - not enough arguments: need input(file, dir, or list) outputFileName label isData"<<endl;
 return 0;
}

std::string input = argv[1];
std::string outputFileName = argv[2];
std::string label = argv[3];
std::string isData_ = argv[4];


system("mkdir -p hists");
system(("mkdir -p hists/"+label).c_str());

std::string year_ = "all";
if(input.find("MC16a") != std::string::npos || input.find("2015-2016.") != std::string::npos) {year_ = "2016"; lumi = 36.20766;}
if(input.find("MC16d") != std::string::npos || input.find("2017.") != std::string::npos) {year_ = "2017"; lumi = 44.3074;}
if(input.find("MC16e") != std::string::npos || input.find("2018.") != std::string::npos) {year_ = "2018"; lumi = 58.4501;}
system(("mkdir -p hists/"+label+"/"+year_).c_str());

bool isData = false;
if(isData_ == "1" || isData_ == "true" || isData_ == "yes" || isData_ == "True" || isData_ == "Yes") isData = true;


std::vector<std::string> list_chain;

if(input.find(".root") != std::string::npos) //a file is given, can contain regex
{
    std::system(("ls "+input+" > "+outputFileName+year_+".txt").c_str());
    std::ifstream inFile_list(outputFileName+year_+".txt");
    std::string str_list;
    while (std::getline(inFile_list, str_list))
    {
        if(str_list.find(".root") != std::string::npos) list_chain.push_back(str_list);
    }
    std::system(("rm "+outputFileName+year_+".txt").c_str());
}

else if (input.find(",") != std::string::npos)//a list is given, separated by ","
{
  int a =0;
  int b=input.find(",");
  while(1)
  {
	std::string thisFile = input.substr(a,b-a);
	list_chain.push_back(thisFile);
	a = b+1;
	if(input.find(",", a) != std::string::npos) b = input.find(",", a);
	else
	{
		b = input.size();
		std::string thisFile = input.substr(a,b-a);
		list_chain.push_back(thisFile);
	}
  }
  
}
else //a directory is given
{
  DIR* dirp = opendir(input.c_str());
  struct dirent * dp;
  while ((dp = readdir(dirp)) != NULL)
  {
	std::string thisFile = dp->d_name;
	if(thisFile.find(".root") == std::string::npos) continue;
	list_chain.push_back((input+"/"+thisFile));
  }

}

if(list_chain.size() < 1) return 0;
cout<<"processing the following input file(s):"<<endl;
for(int i=0; i<list_chain.size(); i++)
{
    cout<<list_chain[i]<<endl;
}

//construct RDataFrame:
ROOT::RDataFrame df("XhhMiniNtuple", list_chain);
auto nEntries = df.Filter("1").Count();
cout<<"total number of events to process: "<<*nEntries<<endl;


ROOT::EnableImplicitMT();


//*********************define additional variables*****************//
auto df_variables = df.Define("lumi", [&]() {return isData ? 1.0 : lumi;}, {})
                      .Define("isData", [&]() {return isData;}, {})
                      .Define("boostedJets_Xbb2020v3_Combine", combineXbb, {"boostedJets_Xbb2020v3_Higgs", "boostedJets_Xbb2020v3_QCD", "boostedJets_Xbb2020v3_Top"})
                      .Define("sortIdxJets", sortObjects, {"boostedJets_pt"})
                      //.Define("sortIdxJets", sortObjects, {"boostedJets_Xbb2020v3_Higgs"})
                      .Define("jet_pt1", leadingVar, {"boostedJets_pt", "sortIdxJets"})
                      .Define("jet_m1", leadingVar, {"boostedJets_m", "sortIdxJets"})
                      .Define("jet_XbbHiggs1", leadingVar, {"boostedJets_Xbb2020v3_Higgs", "sortIdxJets"})
                      .Define("jet_XbbQCD1", leadingVar, {"boostedJets_Xbb2020v3_QCD", "sortIdxJets"})
                      .Define("jet_XbbTop1", leadingVar, {"boostedJets_Xbb2020v3_Top", "sortIdxJets"})
                      .Define("jet_XbbCombine1", leadingVar, {"boostedJets_Xbb2020v3_Combine", "sortIdxJets"})
                      .Define("jet_pt2", subleadingVar, {"boostedJets_pt", "sortIdxJets"})
                      .Define("jet_m2", subleadingVar, {"boostedJets_m", "sortIdxJets"})
                      .Define("jet_XbbHiggs2", subleadingVar, {"boostedJets_Xbb2020v3_Higgs", "sortIdxJets"})
                      .Define("jet_XbbQCD2", subleadingVar, {"boostedJets_Xbb2020v3_QCD", "sortIdxJets"})
                      .Define("jet_XbbTop2", subleadingVar, {"boostedJets_Xbb2020v3_Top", "sortIdxJets"})
                      .Define("jet_XbbCombine2", subleadingVar, {"boostedJets_Xbb2020v3_Combine", "sortIdxJets"});

//*********************define cuts*****************//
auto df_CutWeight = df_variables.Define("CutWeight", "isData ? lumi : lumi*event_weight");
auto df_yield = df_CutWeight.Define("yield", [&]() {return 0.0;}, {});
auto df_nJets = df_yield.Filter("nboostedJets>1");
auto df_JetPt = df_nJets.Filter("jet_pt1 > 300.0 && jet_pt2> 300.0");
auto df_JetMass = df_JetPt.Filter("jet_m1 > 105.0 && jet_m1 < 135.0 && jet_m2 > 105.0 && jet_m2 < 135.0");
auto df_JetXbb1 = df_JetMass.Filter("jet_XbbCombine1 > 3.09"); 
auto df_JetXbb2 = df_JetXbb1.Filter("jet_XbbCombine2 > 3.09"); 
//************************************************//


//*******select cuts to save histogram************//
std::vector<cut_type> cuts_temp;

cuts_temp.push_back((cut_type){"CutWeight", "CutWeight", df_CutWeight});
cuts_temp.push_back((cut_type){"nJets", "CutWeight", df_nJets});
cuts_temp.push_back((cut_type){"JetPt", "CutWeight", df_JetPt});
cuts_temp.push_back((cut_type){"JetMass", "CutWeight", df_JetMass});
cuts_temp.push_back((cut_type){"JetXbb1", "CutWeight", df_JetXbb1});
cuts_temp.push_back((cut_type){"JetXbb2", "CutWeight", df_JetXbb2});
//************************************************//


//******define histograms to save for each cut****//
std::vector<histogram_type> histograms;

histograms.push_back((histogram_type){"yield", "; yield; Events", 1, 0., 1., "yield"});
histograms.push_back((histogram_type){"jet_pt1", "; p_{T}^{j1} (GeV); Events", 300, 200., 1100., "jet_pt1"});
histograms.push_back((histogram_type){"jet_m1", "; m_{j1} (GeV); Events", 300, 0., 300., "jet_m1"});
histograms.push_back((histogram_type){"jet_XbbHiggs1", "; Xbb2020v3_Higgs (j1); Events", 300, 0., 1., "jet_XbbHiggs1"});
histograms.push_back((histogram_type){"jet_XbbQCD1", "; Xbb2020v3_QCD (j1); Events", 300, 0., 1., "jet_XbbQCD1"});
histograms.push_back((histogram_type){"jet_XbbTop1", "; Xbb2020v3_Top (j1); Events", 300, 0., 1., "jet_XbbTop1"});
histograms.push_back((histogram_type){"jet_XbbCombine1", "; Xbb2020v3_Combine (j1); Events", 300, -2., 7., "jet_XbbCombine1"});
histograms.push_back((histogram_type){"jet_pt2", "; p_{T}^{j2} (GeV); Events", 300, 200., 1100., "jet_pt2"});
histograms.push_back((histogram_type){"jet_m2", "; m_{j2} (GeV); Events", 300, 0., 300., "jet_m2"});
histograms.push_back((histogram_type){"jet_XbbHiggs2", "; Xbb2020v3_Higgs (j2); Events", 300, 0., 1., "jet_XbbHiggs2"});
histograms.push_back((histogram_type){"jet_XbbQCD2", "; Xbb2020v3_QCD (j2); Events", 300, 0., 1., "jet_XbbQCD2"});
histograms.push_back((histogram_type){"jet_XbbTop2", "; Xbb2020v3_Top (j2); Events", 300, 0., 1., "jet_XbbTop2"});
histograms.push_back((histogram_type){"jet_XbbCombine2", "; Xbb2020v3_Combine (j2); Events", 300, -2., 7., "jet_XbbCombine2"});
//************************************************//


std::vector<cut_type> cuts;

for(int ic=0; ic<cuts_temp.size(); ic++)
{
    cuts.push_back(cuts_temp[ic]);
}

auto df_yield2 = cuts[0].node.Define("yield2", [&]() {return 0.0;}, {});

//************************************************//

//save histograms for all cuts:
TFile *outfile = new TFile(("hists/"+label+"/"+year_+"/"+outputFileName).c_str(), "recreate");
auto hist_yield_root = df_yield2.Histo1D({cuts[0].cutname+"__"+histograms[0].savename, histograms[0].title, histograms[0].nbins, histograms[0].xlow, histograms[0].xhigh}, "yield2", cuts[0].weight);
std::vector<decltype(hist_yield_root)> hists;
hists.push_back(hist_yield_root);
for(int ic=0; ic<cuts.size(); ic++)
{
    for(int ih=0; ih<histograms.size(); ih++)
    {
        if(cuts[ic].cutname == "CutWeight" && histograms[ih].varname =="yield") continue;
        auto hist_temp = cuts[ic].node.Histo1D({cuts[ic].cutname+"__"+histograms[ih].savename, histograms[ih].title, histograms[ih].nbins, histograms[ih].xlow, histograms[ih].xhigh}, histograms[ih].varname, cuts[ic].weight);
        hists.push_back(hist_temp);
    }
}
for(int idx=0; idx<hists.size(); idx++) hists[idx]->Write();
outfile->Close();

return 0;
}
